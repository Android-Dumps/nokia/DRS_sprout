#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_DRS_sprout.mk

COMMON_LUNCH_CHOICES := \
    omni_DRS_sprout-user \
    omni_DRS_sprout-userdebug \
    omni_DRS_sprout-eng
