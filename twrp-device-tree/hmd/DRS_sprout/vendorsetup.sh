#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_DRS_sprout-user
add_lunch_combo omni_DRS_sprout-userdebug
add_lunch_combo omni_DRS_sprout-eng
