#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from DRS_sprout device
$(call inherit-product, device/hmd/DRS_sprout/device.mk)

PRODUCT_DEVICE := DRS_sprout
PRODUCT_NAME := lineage_DRS_sprout
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia 3.4
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="DoctorStrange_00WW-user 12 SKQ1.220119.001 00WW_3_500 release-keys"

BUILD_FINGERPRINT := Nokia/DoctorStrange_00WW/DRS_sprout:12/SKQ1.220119.001/00WW_3_500:user/release-keys
