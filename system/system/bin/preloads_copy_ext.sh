#!/system/bin/sh
umask 022
if [ $# -eq 1 ]; then
# Where system_other is mounted that contains the preloads dir
mountpoint=/vendor/odm/preload_apps/
log -p i -t preloads_copy_ext "install from $mountpoint"
# Parallelize by copying subfolders and files in the background
find $mountpoint -type f -print0 | while read -d $'\0' file; do
  log -p i -t preloads_copy_ext "install $file";
  /system/bin/pm install $file
done
# Wait for jobs to finish
# wait
log -p i -t preloads_copy_ext "install complete"
exit 0
else
log -p e -t preloads_copy_ext "Usage: preloads_copy.sh <system-mount-point>"
exit 1
fi
